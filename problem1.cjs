const url = "https://jsonplaceholder.typicode.com/users"; //url of users
fetch(url) //fetching data
  .then((response) => {
    return response.text(); //returning promise resolving to text representation
  })
  .then((data) => {
    console.log(data); //printing data
  })
  .catch((error) => {
    console.log(error); //if error occurs printing error
  });
