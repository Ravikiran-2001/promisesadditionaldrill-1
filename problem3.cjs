const userUrl = "https://jsonplaceholder.typicode.com/users"; //url of users
const todosUrl = "https://jsonplaceholder.typicode.com/todos"; //url of todos
fetch(userUrl) //fetching data
  .then((response) => {
    return response.text(); //returning promise resolving to text representation
  })
  .then((data) => {
    console.log("user:", data); //printing user data
    return fetch(todosUrl); //fetching todos
  })
  .then((response) => {
    return response.text(); //returning promise resolving to text representation
  })
  .then((data) => {
    console.log("todos:", data); //printing todo data
  })
  .catch((error) => {
    console.log(error); //if error occurs printing error
  });
