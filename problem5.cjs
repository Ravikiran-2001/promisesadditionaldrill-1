const url = "https://jsonplaceholder.typicode.com/todos/1"; //url of first todos
fetch(url) //fetching data
  .then((response) => {
    return response.json(); //returning promise resolving to json representation
  })
  .then((data) => {
    return fetch(
      `https://jsonplaceholder.typicode.com/todos?userId=${data.userId}` //adding userid to url and fetching data
    );
  })
  .then((response) => {
    return response.json(); //returning promise resolving to json representation
  })
  .then((data) => {
    console.log(data); //printing data
  })
  .catch((error) => {
    console.log(error); //if error occurs printing error
  });
