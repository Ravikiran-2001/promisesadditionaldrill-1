const url = "https://jsonplaceholder.typicode.com/users"; //url of user
fetch(url) //fetching user data
  .then((response) => {
    return response.json(); //returning promise resolving to json representation
  })
  .then((data) => {
    const fetchPromises = data.map((element) => {
      //using map to iterate inside data
      return fetch(
        `https://jsonplaceholder.typicode.com/users?id=${element.id}` //adding id to url and fetching data
      ).then((response) => {
        return response.json(); //returning promise resolving to json representation
      });
    });
    return Promise.all(fetchPromises); //main purpose of using all is it will wait until all promises are resolved , if we don't use data will be returned randomly which will give random sequence in output
  })
  .then((data) => {
    console.log(data); //printing data
  })
  .catch((error) => {
    console.log(error); //if error occurs printing error
  });
